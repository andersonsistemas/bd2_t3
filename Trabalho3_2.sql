﻿select * from departamento;
select * from dependente;
select * from dept_locais;
select * from func_proj;
select * from funcionario;
select * from projeto;

select * from funcionario f
where f.salario>100 and f.salario<30000;


 -- TRIGGER 001  OK --
  CREATE OR REPLACE FUNCTION auditoria_funcionarios()
  RETURNS trigger AS $auditoria_trigger$
  BEGIN
  INSERT INTO funcionario_auditoria
  VALUES(current_timestamp,NEW.nome,NEW.sobrenome,NEW.cpf,(select dnome from departamento d where NEW.dno=d.dnumero));
  RETURN NEW;
  END;
  $auditoria_trigger$ LANGUAGE plpgsql; 

-- TRIGGER PARA AUDITORIA CHAMANDO UMA FUNCAO
  CREATE TRIGGER audit_log_trigger
  AFTER INSERT ON funcionario
  FOR EACH ROW
  EXECUTE PROCEDURE auditoria_funcionarios();

--CRIAÇÃO DE UMA TABELA DE AUDITORIA DE FUNCIONARIOS
CREATE TABLE funcionario_auditoria (
      data timestamp NOT NULL,
      nome varchar NOT NULL,
      sobrenome varchar ,
      cpf varchar NOT NULL,
      departamento TEXT NOT NULL
  );
  
--TESTE TRG 001
INSERT INTO funcionario VALUES
  ('Jhosef','C','Almeida','123444333','10-JUN-1985','7676 Blue Air, Saymonich, CA','M',81000,null,4);
SELECT * from funcionario_auditoria
-- END TRIGGER 001


-- TRIGGER 002  -- OK
--trigger para reajuste dos funcionarios da administracao
--bonus salarial de 1000 reais
    CREATE TRIGGER bonus_departamento_trig
    AFTER UPDATE ON funcionario
    FOR EACH ROW
    EXECUTE PROCEDURE bonus_departamento(); 

CREATE OR REPLACE FUNCTION bonus_departamento()  
  RETURNS trigger AS  
$$  
   BEGIN
    IF NEW.dno = 4 THEN
       NEW.salario = NEW.salario + 1000;
    END IF;
END;
$$  
LANGUAGE 'plpgsql';
-- END TRG 002


-- TRIGGER 003 -- OK
--trigger para bonus salarial após 50 horas
CREATE TRIGGER bonus_proj_trig
    AFTER UPDATE ON func_proj
    FOR EACH ROW
    EXECUTE PROCEDURE bonus_proj(); 

CREATE OR REPLACE FUNCTION bonus_proj()  
  RETURNS trigger AS  
$$  
   BEGIN
   IF NEW.hours > 50 THEN
	UPDATE funcionario f
        SET f.salario = f.salario + 500;
    END IF;  
    RETURN NEW;  
END;  
$$  
LANGUAGE 'plpgsql';
-- END TRG 003


--FUNCAO 001 - OK -
-- Funcao para Retornar o NOME de um Funcionario, passando o cpf como parâmetro
create or replace function nome_cliente_by_cpf(text) returns text as $$ 
declare
     registro record;
begin
     select into registro * from funcionario where cpf = $1;
     if not found then
     raise exception 'Nao existe Funcionario para o CPF informado!';
     end if;
     return r.nome;
end;
$$
language 'plpgsql';

--TESTE FUNCAO CLIENTE POR CPF
select nome_cliente_by_cpf('000112233');


--FUNCAO 002 DATADIFF - OK -
-- FUNCAO CALCULAR A DIFERENCA ENTRE DUAS DATAS
CREATE OR REPLACE FUNCTION datediff(type VARCHAR, date_from DATE, date_to DATE)
RETURNS INTEGER LANGUAGE plpgsql AS $$
DECLARE age INTERVAL;
BEGIN
    age := age(date_to, date_from);
    CASE type
        WHEN 'month' THEN
            RETURN date_part('year', age) * 12 + date_part('month', age);
        WHEN 'year' THEN
            RETURN date_part('year', age);
        ELSE
            RETURN (date_to - date_from)::int;
    END CASE;
END;
$$;

-- TESTE FUNCAO DATEDIFF
/* CALCULAR DUAS DATAS EM DIAS */
SELECT datediff('day', '2017-02-14', '2017-07-09');
/* CALCULAR DUAS DATAS EM MESES */
SELECT datediff('month', '2015-02-14', '2016-01-03');
/* CALCULAR DUAS DATAS EM ANOS */
SELECT datediff('year', '2015-02-14', '2016-01-03');

/* DIFERENCA DE DIAS ENTRE A DATA INFORMADA E A DATA ATUAL */
--DIA
SELECT datediff('day', '2017-07-01', NOW()::date);
--MES
SELECT datediff('month', '2017-07-01', NOW()::date);
--ANO
SELECT datediff('year', '2017-07-01', NOW()::date);
--END TESTE DATEDIFF



--VIEW Pessoas Departamento ADMINISTRACAO
create view pessoas_departamento as
select f.nome,f.sobrenome,f.cpf,d.dnome as departamento from funcionario f,departamento d
where f.dno=d.dnumero and d.dnome='Administration'

--TESTE VIEW Departamento ADMINISTRACAO
select * from pessoas_departamento

-- TRIGGER ATUALIZA_VIEW DE ACESSO AO DEPARTAMENTO
create trigger Atualiza_Pessoas_Deptartamento
instead of update of dnome on pessoas_departamento
for each row
begin
  update departamento
  set dnome = New.dnome
  where dnumero = New.dnumero;
end;


-- VIEW FUNCIONARIO PROJETO
create view funcionario_projeto as
select f.nome,f.sobrenome,p.pnome as Projeto,p.plocal as Local from ((funcionario f
inner join func_proj fp on fp.cpf=f.cpf)
inner join projeto p on p.pnumero=fp.pno)
where p.plocal='Stafford'

--TESTE VIEW STAFFORD
select * from funcionario_projeto

-- TRIGGER ATUALIZA_VIEW DE ACESSO AO PROJETO
create trigger Atualiza_Pessoas_Projeto
instead of INSERT ON funcionario_projeto
for each row
EXECUTE PROCEDURE funcionario_projeto(); 


CREATE OR REPLACE FUNCTION funcionario_projeto()  
  RETURNS trigger AS  
$$  
  BEGIN
  update projeto
  set pnome = New.pnome
  where pnumero = New.pnumero;
end;
$$  
LANGUAGE 'plpgsql';
;

--VIEW MATERIALIZADA ATUALIZADA POR TRIGGER

/* RECURSO QUE PERMITE O ARMAZENAMENTO DE VIEWS*/
--CRIACAO DE VIEW MATERIALIZADA
CREATE MATERIALIZED VIEW view_materializada_funcionario 
AS SELECT * FROM funcionario WHERE dno=1 WITH NO DATA;
--TESTE
SELECT * FROM view_materializada_funcionario;
--ATUALIZA DADOS DA VIEW
REFRESH MATERIALIZED VIEW view_materializada_funcionario;
--APAGA VIEW
DROP MATERIALIZED VIEW view_materializada_funcionario;

--TRIGGER PARA ATUALIZAR A VIEW CASO ACONTEÇA MUDANÇA NA TABELA FUNCIONARIO
CREATE TRIGGER atualiza_view_func_trigger AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
ON funcionario FOR EACH STATEMENT
EXECUTE PROCEDURE atualiza_view_mat_func();

--FUNCAO ACIONADA PELA TRIGGER
CREATE OR REPLACE FUNCTION atualiza_view_mat_func() RETURNS TRIGGER AS $view_func_mat$
   BEGIN
      REFRESH MATERIALIZED VIEW view_materializada_funcionario;
   END;
$view_func_mat$ LANGUAGE plpgsql;




-- VER TODAS AS TRIGGERS
SELECT * FROM pg_trigger;
-- VER TODAS AS TRIGGERS DE UMA TABELA
SELECT tgname FROM pg_trigger, pg_class WHERE tgrelid = pg_class.oid AND relname = 'funcionario'; 
