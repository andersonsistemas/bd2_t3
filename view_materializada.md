Trabalhando com as Views materializadas
Como as Views são apenas para leitura e representação lógica dos dados que estão armazenados nas tabelas do banco de dados, podemos “materializadas”, ou seja, armazená-las fisicamente no disco.

A criação dessas Views, ao invés de novas tabelas, melhora o desempenho em operações de leitura. Os dados das Views materializadas serão então armazenados em uma tabela que pode ser indexada rapidamente quando esta for associada e também em momentos que precisemos atualizar as Views materializadas. Isso ocorre com frequência em Data warehouse e aplicações de Business Intelligence, por exemplo.

Ao contrário da View tradicional, que nos apresentam dados atualizados automaticamente, as Views materializadas precisam de um mecanismo de atualização. A sintaxe é apresentada na Listagem 5.

Listagem 5. Sintaxe básica de uma View materializada.

CREATE MATERIALIZED VIEW table_name
      [ (column_name [, ...] ) ]
      [ WITH ( storage_parameter [= value] [, ... ] ) ]
      [ TABLESPACE tablespace_name ]
      AS query
      [ WITH [ NO ] DATA ]
A instrução CREATE MATERIALIZED VIEW cria View e a consulta é executada para preenche-la. Para atualizar os dados usamos o comando REFRESH MATERIALIZED VIEW. Uma View materializada tem muitas das mesmas propriedades de uma tabela, mas não há suporte para materialização temporária ou geração automática de OIDs.

O parâmetro TABLESPACE tablespace_name é o nome do espaço de tabelas no qual a nova View materializada deve ser criada. Se este não for especificado, a consulta será realizada no default_tablespace.

Com base no exemplo que desenvolvemos nesse artigo, a instrução a seguir cria uma view materializada para a tabela funcionarios:

CREATE MATERIALIZED VIEW view_materializada_funcionario AS SELECT * FROM funcionarios WITH NO DATA;
Para inserir dados nessa view usaremos o código da Listagem 6.

Listagem 6. Inserindo dados na view materializada

INSERT INTO funcionarios (codigo, nome_func, data_entrada, profissao) VALUES (5, 'Gustavo França', '2014-10-11', 'Estagiário');
  INSERT INTO funcionarios (codigo, nome_func, data_entrada, profissao) VALUES (6, 'Mayara Silva', '2015-06-10', 'Analista de testes');
  INSERT INTO funcionarios (codigo, nome_func, data_entrada, profissao) VALUES (7, 'João dos testes', '2011-01-01', 'Gerente de negócios');
  INSERT INTO funcionarios (codigo, nome_func, data_entrada, profissao) VALUES (8, 'Marina França', '2012-03-07', 'Analista de negócios');
  INSERT INTO funcionarios (codigo, nome_func, data_entrada, profissao) VALUES (9, 'Paulo Dionisio', '2013-07-07', 'DBA Sênior');
Agora utilizaremos o comando SELECT para verificarmos se os registros foram realmente inseridos com sucesso:

SELECT * FROM view_materializada_funcionario;
Podemos ver que obtivemos um erro ao tentarmos consultar a nossa View, como mostra a Figura 2.

Visualização de erro ao consultar View materializada

Figura 2. Visualização de erro ao consultar View materializada.

Este erro ocorreu porque a view não se atualizou automaticamente, então precisamos do comando Refresh Materialized View:

REFRESH MATERIALIZED VIEW view_materializada_funcionario;
Feito isso, e em seguida, tentando consultar novamente os dados e assim obtemos êxito.

Com o PostgreSQL 9.4 podemos consultar Views materializadas enquanto são atualizadas, porém, nas versões anteriores isso não é possível. Para esse procedimento utilizamos a palavra-chave CONCURRENTLY, como mostra o comando a seguir:

REFRESH MATERIALIZED VIEW CONCURRENTLY view_materializada_funcionario;
Para que isso ocorra, um índice exclusivo passa a ser necessário para existir na View materializada. Sendo assim, ao executarmos o comando select novamente, teremos todos os dados atualizados, como podemos ver na Figura 3.

Selecionando dados da View Materializada

Figura 3. Selecionando dados da View Materializada.

Para excluir esse tipo de view basta utilizarmos o seguinte comando:

DROP MATERIALIZED VIEW view_materializada_funcionario;
Todos os recursos de otimização são importantes, pois lembre-se que as views são apenas visões dos dados e são carregadas apenas quando necessárias. Mas elas criam uma camada extra para administrar e podem limitar exageradamente, impedindo certas tarefas.

Além disso, não confunda uma view materializada com uma trigger, pois apesar de funcionarem de forma semelhante, a trigger tem muito mais poder sobre a tabela.

Esperamos que tenham gostado. Até a próxima!

Links

Documentação da View
http://www.postgresql.org/docs/9.4/static/sql-createview.html