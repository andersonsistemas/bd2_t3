/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd2_t3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Neo
 */
public class BD2_T3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        getSelect("select * from funcionario");
    }
    
     private static Connection getConexao() throws SQLException
   	{
  		
                //String url = "jdbc:mysql://localhost:3306/bd2_t2?autoReconnect=true&useSSL=false";
   		String url = "jdbc:postgresql://localhost/T3";
   		try
   		{
               Class.forName("org.postgresql.Driver");
                    //Class.forName("com.mysql.jdbc.Driver");    
    		
    	}
  
    	catch (ClassNotFoundException e) 
    	{
    		
    		System.err.println("Postgres jdbc driver not found.");
                e.printStackTrace();
    		System.exit(-1);
    	}
    	Connection conn = DriverManager.getConnection(url, "postgres", "root");
        
    	return conn;	
    }
     
     
     
     
     public static void getSelect(String SQL) {
 
        //String SQL = "SELECT * from funcionario";
 
        try (Connection conn = getConexao();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(SQL)) {
            // exibe resultado
            exibirResultado(rs);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
     
     public static void exibirResultado(ResultSet rs) throws SQLException {
        while (rs.next()) {
            System.out.println(rs.getString("nome") + "\t"
                    + rs.getString("sobrenome") + "\t"
                    + rs.getString("cpf"));
 
        }
    }
     
     
}
